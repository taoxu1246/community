# SIG_DataManagement 
English | [简体中文](./sig-distributeddatamgr_cn.md)

Note: The content of this SIG follows the convention described in OpenHarmony's PMC Management Charter [README](/zh/pmc.md).

## SIG group work objectives and scope

### Build a distributed data management framework for all scenarios of 1+8+N equipment, and provide developers with convenient, efficient, stable and secure data management related services.

### Local database, distributed database, data management service, etc.

### The repository 
- project name:
  - distributeddatamgr_preferences：https://gitee.com/openharmony/distributeddatamgr_preferences
  - distributeddatamgr_relational_store：https://gitee.com/openharmony/distributeddatamgr_relational_store
  - distributeddatamgr_data_share：https://gitee.com/openharmony/distributeddatamgr_data_share
  - distributeddatamgr_kv_store：https://gitee.com/openharmony/distributeddatamgr_kv_store
  - distributeddatamgr_datamgr_service：https://gitee.com/openharmony/distributeddatamgr_datamgr_service
  - distributeddatamgr_data_object：https://gitee.com/openharmony/distributeddatamgr_data_object
  - miscservices_pasteboard：https://gitee.com/openharmony/miscservices_pasteboard

## SIG Members

### Leader
- @gong-a-shi(https://gitee.com/gong-a-shi)

### Committers
- @wbq_sky(https://gitee.com/wbq_sky)
- @purple-ding-gags(https://gitee.com/purple-ding-gags)

### Meetings
 - Meeting time：Every Tuesday at 14:00 o'clock
 - Meeting link：Welink

### Contact (optional)

- Mailing list: dev@openharmony.io
- Zulip group: https://zulip.openharmony.cn
- Wechat group：NA
